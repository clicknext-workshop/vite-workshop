interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];
//Declare Var
let count = 0;
let score = document.createElement('p');
score.innerHTML = 'Current Score: ' + count + '/10';

//Function updateScore
function updateScore() {
  count += 1;
  score.innerHTML = 'Current Score: ' + count + '/ 10';
}

//Initial Score
const div = document.getElementById('exercise2') as HTMLDivElement;
div.appendChild(score);

//Render Questions
questions.forEach((question, index) => {
  let divSub = document.createElement('div');
  let divRadio = document.createElement('div');
  let questionText = document.createElement('p');
  let statusAns = document.createElement('p');
  questionText.innerText = question.question;
  divSub.appendChild(questionText);

  //Render Choices
  question.choices.forEach((choice, choiceIndex) => {
    let sectionRadio = document.createElement('div');
    let radioBtn = document.createElement('input');
    let spanQuestion = document.createElement('span');
    spanQuestion.innerText = choice;
    radioBtn.setAttribute('type', 'radio');
    radioBtn.setAttribute('name', 'question_' + index);
    radioBtn.setAttribute('value', choiceIndex.toString());

    sectionRadio.appendChild(radioBtn);
    divRadio.appendChild(sectionRadio);
    sectionRadio.appendChild(spanQuestion);
  });

  //Create Sumbit Button
  let button = document.createElement('button');
  button.innerHTML = 'Submit';

  //Add Event
  button.addEventListener('click', () => {
    const selectedOption = divSub.querySelector(
      'input[name="question_' + index + '"]:checked'
    ) as HTMLInputElement;
    if (selectedOption) {
      const selectedAnswer = parseInt(selectedOption.value);
      if (selectedAnswer === question.correctAnswer) {
        statusAns.innerText = 'Correct!';
        updateScore();
      }else{
        statusAns.innerText = 'Incorrect!';
      }
    }
  
    // Check if any option is selected
    const anyOptionSelected = divSub.querySelector(
      'input[name="question_' + index + '"]:checked'
    );
  
    // Disable radio buttons and submit button only if an option is selected
    if (anyOptionSelected) {
      // Disable radio buttons
      const radioButtons = divSub.querySelectorAll('input[name="question_' + index + '"]');
      radioButtons.forEach((radioButton) => {
        (radioButton as HTMLInputElement).disabled = true;
      });
  
      // Disable submit button
      button.disabled = true;
    }else{
      alert("please choose an answer first!");
    }
  });

  divSub.appendChild(divRadio);
  divSub.appendChild(button);
  divSub.appendChild(statusAns);
  div.appendChild(divSub);
});
