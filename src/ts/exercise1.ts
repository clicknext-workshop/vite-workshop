interface Menu {
    name: string 
    subMenu: SubMenu[]
  }
  
  interface SubMenu {
    name: string
  }
  
  const menus: Menu[] = [
    {
      name: 'Home',
      subMenu: [],
    },
    {
      name: 'About',
      subMenu: [
        {
          name: 'Company',
        },
        {
          name: 'Team',
        },
      ],
    },
    {
      name: 'Products',
      subMenu: [
        {
          name: 'Electronics',
        },
        {
          name: 'Clothing',
        },
        {
          name: 'Accessories',
        },
      ],
    },
    {
      name: 'Services',
      subMenu: [],
    },
    {
      name: 'Contact',
      subMenu: [
        {
          name: 'Phone',
        },
      ],
    },
    {
      name: 'Blog',
      subMenu: [],
    },
    {
      name: 'Gallery',
      subMenu: [
        {
          name: 'Photos',
        },
        {
          name: 'Videos',
        },
        {
          name: 'Events',
        },
      ],
    },
    {
      name: 'FAQ',
      subMenu: [],
    },
    {
      name: 'Downloads',
      subMenu: [
        {
          name: 'Documents',
        },
        {
          name: 'Software',
        },
      ],
    },
    {
      name: 'Support',
      subMenu: [
        {
          name: 'Help Center',
        },
        {
          name: 'Contact Us',
        },
        {
          name: 'Knowledge Base',
        },
      ],
    },
  ];

const div = document.getElementById('exercise1') as HTMLDivElement;
const menu = document.getElementById('menu');

menus.forEach(element => {
    let li = document.createElement('li');
    li.innerText = element.name;
    let ul = document.createElement('ul');
    element.subMenu.forEach(menuItem => {
        let subIl = document.createElement('li');
        const nameSub = document.createTextNode(menuItem.name);
        subIl.appendChild(nameSub);
        ul.appendChild(subIl);
    })
    li.appendChild(ul);
    menu?.appendChild(li);
});
export { }
